#.rst:
# gmmreg
# -----------
#
# Try to find gmmreg, the librarhy for gaussian mixture model based deformable registration of point clouds
#
# Once done this will define
#
# ::
#
#   GMMREG_FOUND - System has GMMReg
#   GMMREG_INCLUDE_DIR - The GMMReg include directory
#   GMMREG_LIBRARIES - The libraries needed to use GMMReg
#   GMMREG_DEFINITIONS - Compiler switches required for using GMMReg
#   GMMREG_DEMO_EXECUTABLE - The gmmreg_demo program that executes all GMMReg alg using a cmd line interface with ini files

#   I'm considering using the git commit
#   GMMREG_VERSION_STRING - the version of GMMReg found
# Requires cmake 3.0


find_program(GMMREG_DEMO_EXECUTABLE gmmreg_demo
	PATHS ${GMMREG_BIN_DIR}
	PATH_SUFFIXES Release Debug bin)


if(EXISTS ${GMMREG_DEMO_EXECUTABLE})

	if(NOT EXISTS ${GMMREG_BIN_DIR})
		get_filename_component(GMMREG_BIN_DIR ${GMMREG_DEMO_EXECUTABLE} DIRECTORY CACHE)

	endif()
endif()
	
find_path(GMMREG_INCLUDE_DIR gmmreg_api.h
	PATHS ${GMMREG_SOURCE_DIR}
	PATH_SUFFIXES C++)
	
if(EXISTS ${GMMREG_INCLUDE_DIR})
	if(NOT EXISTS ${GMMREG_SOURCE_DIR})
		get_filename_component(GMMREG_SOURCE_DIR ${GMMREG_INCLUDE_DIR} DIRECTORY CACHE)
	endif()
endif()
	
	
find_library(GMMREG_LIBRARIES gmmreg_api
	PATHS ${GMMREG_BIN_DIR})

if(EXISTS ${GMMREG_LIBRARIES})
	if(NOT EXISTS ${GMMREG_BIN_DIR})
		get_filename_component(GMMREG_BIN_DIR ${GMMREG_LIBRARIES} DIRECTORY CACHE)
	endif()
endif()

set(GMMREG_DEFINITIONS Openmp CACHE STRING "Complier Flags")

# handle QUIETLY and REQUIRED and set GMMREG_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GMMREG
                                  REQUIRED_VARS GMMREG_BIN_DIR GMMREG_SOURCE_DIR GMMREG_DEMO_EXECUTABLE GMMREG_INCLUDE_DIR GMMREG_LIBRARIES GMMREG_DEFINITIONS
								  FAIL_MESSAGE "Could not find package GMMREG package.  Please set GMMREG_BIN_DIR and GMMREG_SOURCE_DIR")	