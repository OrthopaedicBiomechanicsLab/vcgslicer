#The idea is that this test is run from the command line with something like
# /c/S4Dvs2013/Slicer-build/Slicer.exe --python-script testVcgSurfaceFilters.py
# /c/S4Rvs2013/Slicer-build/Slicer.exe --python-script testVcgSurfaceFilters.py

import time

slicer.util.loadLabelVolume("C:/Mike/Git/calavera-3d-slicer/vcgSurfaceFilters/Data/Input/skull_label.nrrd")
skullLabelNode = getNode('skull_label')



modelMakerParameters={}
modelMakerParameters['InputVolume'] = skullLabelNode.GetID()
modelMakerParameters['Labels'] = 1
modelMakerParameters['Name'] = 'Model'

modelHNode = slicer.mrmlScene.CreateNodeByClass('vtkMRMLModelHierarchyNode')
modelHNode = slicer.mrmlScene.AddNode(modelHNode)

modelMakerParameters['ModelSceneFile'] = modelHNode.GetID()
modelMakerParameters['Smooth'] = 15
modelMakerParameters['Labels'] = [1]
modelMakerParameters['Pad'] = False
modelMakerParameters['GenerateAll'] = False
modelMakerParameters['SkipUnNamed'] = False
modelMakerParameters['SplitNormals'] = False

modelMaker = slicer.modules.modelmaker
node = slicer.cli.run(modelMaker, None, modelMakerParameters,True)

models = slicer.mrmlScene.GetNodesByClass('vtkMRMLModelNode')
numberOfModels = models.GetNumberOfItems()
newModel = models.GetItemAsObject(numberOfModels-1)


inputModel = newModel

outputModel = slicer.mrmlScene.CreateNodeByClass('vtkMRMLModelNode')
outputModel = slicer.mrmlScene.AddNode(outputModel)


vcgFilter = "FP_TWO_STEP_SMOOTH"
smoothingSteps = 3
featureAngle = 60
normalSmoothingSteps = 5
vertexFittingSteps = 5

vcgParameters = {}
vcgParameters["smoothingSteps"] = smoothingSteps
vcgParameters["featureAngle"] = featureAngle
vcgParameters["normalSmoothingSteps"] = normalSmoothingSteps
vcgParameters["vertexFittingSteps"] = vertexFittingSteps
vcgParameters["model"] = inputModel.GetID()
vcgParameters["filteredModel"] = outputModel.GetID()
vcgParameters["vcgFilter"] = vcgFilter
#default is FP_TWO_STEP_SMOOTH; other element is FP_TAUBIN_SMOOTH
vcgFiltersModule = slicer.modules.vcgsurfacefilters
slicer.cli.run(vcgFiltersModule, None, vcgParameters)

