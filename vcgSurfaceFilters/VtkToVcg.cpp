#include "VtkToVcg.h"
#include "vtkIdList.h"
#include <memory>

VtkToVcg::VtkToVcg(vtkPolyData* polyData) {
  setNewVtkPolyData(polyData);
}
bool VtkToVcg::setNewVtkPolyData(vtkPolyData* polyData)
{

  vtkPoints* points = polyData->GetPoints();
  unsigned int nPoints = unsigned(polyData->GetNumberOfPoints());
  //unsigned int nTriangles = polyData->GetNumberOfCells();
  unsigned int nTriangles = polyData->GetNumberOfPolys();
  unsigned int nStrips = polyData->GetNumberOfStrips();
  if (nStrips > 0)
  {
	  throw "setNewVtkPolyData can only process polydata made up triangle, consider running vtkTriangleFilter first";
  }

  vtkIdType nDim = 3;

  vtkDataArray* pointDataVtk = points->GetData();
  
  float* pointDataC = static_cast<float*>(pointDataVtk->GetVoidPointer(0));
  //matrix.set_size(nPoints,nDim);
  
  
  SelfType::VertexIterator vi = vcg::tri::Allocator<SelfType>::AddVertices(*this, nPoints);
  SelfType::FaceIterator fi = vcg::tri::Allocator<SelfType>::AddFaces(*this, nTriangles);
  //I need to free
  SelfType::VertexPointer* ivp = new SelfType::VertexPointer[nPoints];

  
  for (int i = 0;i < nPoints; i++) {
        
	ivp[i] = &*vi; vi->P()= VcgTriMeshType::CoordType ( pointDataC[i*nDim + 0], pointDataC[i*nDim + 1], pointDataC[i*nDim + 2]); ++vi;
  }
  
 
  vtkIdList* pointIds = vtkIdList::New();
  for(int triangleIndex=0;triangleIndex<nTriangles;triangleIndex++) {
	
	pointIds = vtkIdList::New();
	//int triangleIds = polyData->GetCellPoints(triangleIndex, nDim, pointIds);
	polyData->GetCellPoints(triangleIndex, pointIds);
	  //cout << "polyData->GetCellPoints(triangleIndex, nDim, pointIds) " << polyData->GetCellPoints(0, 3, pointIds) << std::endl;
	
	int pointerIndex = pointIds->GetId(0);
	fi->V(0)=ivp[pointIds->GetId(0)];
	
	//     ***** 
	//ivp[1]=&*vi; vi->P()=MyMesh::CoordType ( 1.0, 0.0, 0.0); ++vi;
	//ivp[2]=&*vi; vi->P()=MyMesh::CoordType ( 0.0, 1.0, 0.0); ++vi;
	// i need to figure out how the triangle are encoded in vtkpolydata
	
	fi->V(1) = ivp[pointIds->GetId(1)];
	fi->V(2) = ivp[pointIds->GetId(2)];
	fi++;
	pointIds->Delete();
  }
  //this->vn = vert.size();

  return true;
}
