#ifndef __VTK_VCG_H
#define __VTK_VCG_H

#include "vcgTriMesh.h"
#include <vtkPolyData.h>


typedef vcg::Box3<MESHLAB_SCALAR>     Box3m;
typedef vcg::Matrix44<MESHLAB_SCALAR> Matrix44m;



//CMeshO with extras
class VtkToVcg    : public VcgTriMeshType
{
public :
	typedef VtkToVcg SelfType;
	typedef  VcgTriMeshType ParentType;


    int sfn;    //The number of selected faces.
    int svn;    //The number of selected vertices.
    Matrix44m Tr; // Usually it is the identity. It is applied in rendering and filters can or cannot use it. (most of the filter will ignore this)
	
	bool setNewVtkPolyData(vtkPolyData* polyDataModel);
	VtkToVcg(vtkPolyData* polyDataModel);

    const Box3m &trBB()
    {
        static Box3m bb;
        bb.SetNull();
        bb.Add(Tr,bbox);
        return bb;
    }
};

#endif