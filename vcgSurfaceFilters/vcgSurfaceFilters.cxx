#include <vtkPolyDataReader.h>
#include <vtkPolyDataWriter.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtksys/SystemTools.hxx>
#include <vtkDebugLeaks.h>
#include <vtkTriangleFilter.h>


#include "VtkToVcg.h"
#include "vtkImportVcgMesh.h"

#include <vcg/complex/algorithms/clean.h>
#include <vcg/complex/algorithms/smooth.h>
#include <vcg/complex/algorithms/crease_cut.h>
#include <vcg/complex/algorithms/update/topology.h>

#include "vcgSurfaceFiltersCLP.h"

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//
int main( int argc, char * argv[] )
{
  PARSE_ARGS;

  vtkDebugLeaks::SetExitError(true);

  vtkSmartPointer<vtkPolyData> polyDataModel = NULL;
  
  // do we have vtk or vtp models?
  std::string extension = vtksys::SystemTools::LowerCase(
    vtksys::SystemTools::GetFilenameLastExtension(model) );
  if( extension.empty() )
    {
    std::cerr << "Failed to find an extension for " << model << std::endl;
    return EXIT_FAILURE;
    }

  // read the first poly data
  if( extension == std::string(".vtk") )
    {
    vtkPolyDataReader* pdReaderModel = vtkPolyDataReader::New();
    pdReaderModel->SetFileName(model.c_str() );
    pdReaderModel->Update();
    polyDataModel = pdReaderModel->GetOutput();
    }
  else if( extension == std::string(".vtp") )
    {
    vtkXMLPolyDataReader* pdxReaderModel = vtkXMLPolyDataReader::New();
    pdxReaderModel->SetFileName(model.c_str() );
    pdxReaderModel->Update();
    polyDataModel = pdxReaderModel->GetOutput();
    }
  
  //this check should be figured out and put back
  /*
  if( readerScene->GetProducer()->GetErrorCode() != 0 )
    {
    std::cerr << "Failed to read scene " << scene << std::endl;
    return EXIT_FAILURE;
    }
  */
  
  //Export to VCG for processing
  //ensure only triangle are passed
  vtkSmartPointer<vtkPolyData> polyDataModelOnlyTriangles = NULL;

  vtkSmartPointer<vtkTriangleFilter> triangleFilter = vtkTriangleFilter::New();
  triangleFilter->SetInputData(polyDataModel);
  triangleFilter->Update();

  std::shared_ptr<VtkToVcg> vcgMesh = std::make_shared<VtkToVcg>(triangleFilter->GetOutput());

  ofstream debugText;
  debugText.open("debug.txt");
  debugText << "vn=" << vcgMesh->VN() << " fn=" << vcgMesh->FN() << std::endl;

  

  //processing
  //case FP_TWO_STEP_SMOOTH:

  try 
  {
	  vcg::tri::Allocator<VtkToVcg>::CompactFaceVector(*vcgMesh);
	  vcg::tri::Allocator<VtkToVcg>::CompactVertexVector(*vcgMesh);
	  //vcg::tri::Clean<VtkToVcg>::RemoveUnreferencedVertex(*vcgMesh);
	  //vcg::tri::UpdateSelection<VtkToVcg>::VertexFromFaceStrict(*vcgMesh);
	  

  }
  catch (...)
  {
	  ofstream debugText;
	  debugText.open("debug.txt");
	  debugText << "exception after initial removing of unreferenced vertecies and updating selection" << std::endl;
	  //vcgMesh->
	  //filteredPolyData->Print(debugText);
	  debugText.close();
	  throw "exception after initial removing of unreferenced vertecies and updating selection";
  }

  int stepSmoothNum = smoothingSteps;
  // sigma==0 all is smoothed
  // sigma==1 nothing is smoothed
  
  float sigma = cos(vcg::math::ToRad(featureAngle));
  if (sigma<0) sigma = 0;
  int stepNormalNum = normalSmoothingSteps;
  int stepFitNum = vertexFittingSteps;
  bool selectedFlag = false;


  debugText << "vn=" << vcgMesh->VN() << " fn=" << vcgMesh->FN() << std::endl;
  debugText << "stepNormalNum:" << stepNormalNum << " sigma:" << sigma << " stepFitNum:" << stepFitNum << " selectedFlag:" << selectedFlag << endl;
  try
  {
	  vcgMesh->face.EnableVFAdjacency();
	  vcgMesh->vert.EnableVFAdjacency();
  }
  catch (...)
  {
	  ofstream debugText;
	  debugText.open("debug.txt");
	  debugText << "exception after EnableVFAdjacency" << std::endl;
	  //vcgMesh->
	  //filteredPolyData->Print(debugText);
	  debugText.close();
	  throw "exception after EnableVFAdjacency";
  }
  //ofstream debugText;
  //debugText.open("debug.txt");
  debugText << "vn=" << vcgMesh->VN() << " fn=" << vcgMesh->FN() << std::endl;
  debugText << "stepNormalNum:" << stepNormalNum << " sigma:" << sigma << " stepFitNum:" << stepFitNum << " selectedFlag:" << selectedFlag << endl;
  try
  {
	  vcg::tri::UpdateTopology< VtkToVcg >::VertexFace(*vcgMesh);
  }
  catch (...)
  {
	  ofstream debugText;
	  debugText.open("debug.txt");
	  debugText << "exception after updateTopology" << std::endl;
	  //vcgMesh->
	  //filteredPolyData->Print(debugText);
	  debugText.close();
	  throw "exception after updateTopology";
  }
  //ofstream debugText;
  //debugText.open("debug.txt");
  debugText << "vn=" << vcgMesh->VN() << " fn=" << vcgMesh->FN() << std::endl;
  debugText << "stepNormalNum:" << stepNormalNum << " sigma:" << sigma << " stepFitNum:" << stepFitNum << " selectedFlag:" << selectedFlag << endl;

  try
  {
	  for (int i = 0; i < stepSmoothNum; ++i)
	  {
		  vcg::tri::UpdateNormal<VtkToVcg>::PerFaceNormalized(*vcgMesh);
		  
		  vcg::tri::Smooth<VtkToVcg>::VertexCoordPasoDoble(*vcgMesh, stepNormalNum, sigma, stepFitNum, selectedFlag);
	  }
  }
  catch (vcg::MissingComponentException e)
  {
	  debugText << e.what() << std::endl;
	  throw e.what();
  }
  catch (vcg::MissingCompactnessException e)
  {
	  debugText << e.what() << std::endl;
	  debugText << "m.vert.size()=" << vcgMesh->vert.size() << " vs m.vn" << vcgMesh->vn << std::endl;
	  debugText << "m.edge.size()=" << vcgMesh->edge.size() << " vs m.en" << vcgMesh->en << std::endl;
	  debugText << "m.face.size()=" << vcgMesh->face.size() << " vs m.fn" << vcgMesh->fn << std::endl;
	  throw e.what();
  }
  catch (vcg::MissingPolygonalRequirementException e)
  {
	  debugText << e.what() << std::endl;
	  throw e.what();
  }
  catch (vcg::MissingPreconditionException e)
  {
	  debugText << e.what() << std::endl;
	  throw e.what();
  }
  catch (vcg::MissingTriangularRequirementException e)
  {
	  debugText << e.what() << std::endl;
	  throw e.what();
  }
  catch (...)
  {
	 
	  
	  debugText << "exception after updateNormal and smooth" << std::endl;
	  debugText << "vn=" << vcgMesh->VN() << " fn=" << vcgMesh->FN() << std::endl;
	  //vcgMesh->
	  //filteredPolyData->Print(debugText);
	  debugText.close();
	  throw "exception after updateNormal and smooth";
  }


  //return from VCG for writing back to slicer
  vtkSmartPointer<vtkImportVcgMesh> vcgImporter = vtkSmartPointer<vtkImportVcgMesh>::New();
  vcgImporter->VcgMesh = vcgMesh;
  
  vtkSmartPointer<vtkPolyData> filteredPolyData;// = vtkSmartPointer<vtkPolyData>::New();

  vcgImporter->Update();
  filteredPolyData = vcgImporter->GetOutput();


  


  //filteredPolyData = polyDataModel;

  // write the output
  extension = vtksys::SystemTools::LowerCase( vtksys::SystemTools::GetFilenameLastExtension(filteredModel) );
  if( extension.empty() )
    {
    std::cerr << "Failed to find an extension for " << filteredModel << std::endl;
    return EXIT_FAILURE;
    }
  if( extension == std::string(".vtk") )
    {
    vtkPolyDataWriter *pdWriter = vtkPolyDataWriter::New();
    pdWriter->SetFileName(filteredModel.c_str() );
	pdWriter->SetInputData(filteredPolyData);
    pdWriter->Write();
    pdWriter->Delete();
    }
  else if( extension == std::string(".vtp") )
    {
    vtkXMLPolyDataWriter *pdWriter = vtkXMLPolyDataWriter::New();
    pdWriter->SetIdTypeToInt32();
    pdWriter->SetFileName(filteredModel.c_str() );
    pdWriter->SetInputData(filteredPolyData);
    pdWriter->Write();
    pdWriter->Delete();
    }

  // clean up
  //add->Delete();
  //readerModel->GetProducer()->Delete();
  //readerScene->GetProducer()->Delete();
  return EXIT_SUCCESS;
}