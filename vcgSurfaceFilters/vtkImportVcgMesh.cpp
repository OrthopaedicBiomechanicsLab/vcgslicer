#include "vtkImportVcgMesh.h"

#include "vtkCellArray.h"
#include "vtkMath.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkCleanPolyData.h"

vtkStandardNewMacro(vtkImportVcgMesh);

vtkImportVcgMesh::vtkImportVcgMesh()
{
  
  this->SetNumberOfInputPorts(0);
}

int vtkImportVcgMesh::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **vtkNotUsed(inputVector),
  vtkInformationVector *outputVector)
{
  // get the info object
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the ouptut
  vtkPolyData *output = vtkPolyData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  vtkIdType numPolys, numPts;
  double x[3];
  int i, j;
  vtkIdType pts[3];

  /* delete
  double theta, deltaRadius;
  double cosTheta, sinTheta;
  
  

    delete end
  */ 

  // Set things up; allocate memory
  //
  vtkPoints *newPoints;
  //vtkPolyData *newPolys;
  numPts = VcgMesh->VertexNumber();
  numPolys = VcgMesh->SimplexNumber();
  newPoints = vtkPoints::New();

  // Set the desired precision for the points in the output.
  //based on how I implimented vtktovcg I've forced float, probably this isn't right
  //if(this->OutputPointsPrecision == vtkAlgorithm::DOUBLE_PRECISION)
  //  {
  //  newPoints->SetDataType(VTK_DOUBLE);
  //  }
  //else
  //  {
    newPoints->SetDataType(VTK_FLOAT);
  //  }

  newPoints->Allocate(numPts);

  vtkCellArray *newPolys;
  newPolys = vtkCellArray::New();
  newPolys->Allocate(newPolys->EstimateSize(numPolys,3));

  /*VcgMeshType::FaceIterator fi = vcgMesh.face.begin();


  VcgMeshType::VertexPointer ivp;
  
  MyMesh::FaceIterator fi = vcg::tri::Allocator<MyMesh>::AddFaces(m,1);
  MyMesh::VertexPointer ivp[4];
  ivp[0]=&*vi; vi->P()=MyMesh::CoordType ( 0.0, 0.0, 0.0); ++vi;
  ivp[1]=&*vi; vi->P()=MyMesh::CoordType ( 1.0, 0.0, 0.0); ++vi;
  ivp[2]=&*vi; vi->P()=MyMesh::CoordType ( 0.0, 1.0, 0.0); ++vi;
  fi->V(0)=ivp[0];
  fi->V(1)=ivp[1];
  fi->V(2)=ivp[2];*/


  /*for (VcgMeshType::VertexIterator vi = VcgMesh->vert.begin(); vi != VcgMesh->vert.end(); ++vi)
  {
	  
	  newPoints->InsertNextPoint(vi->P()[0], vi->P()[1], vi->P()[2]);
  }*/


  //  Create connectivity
  //
  for (VcgMeshType::FaceIterator fi = VcgMesh->face.begin(); fi != VcgMesh->face.end(); ++fi)
  {
	  pts[0] = newPoints->InsertNextPoint(fi->V(0)->P()[0], fi->V(0)->P()[1], fi->V(0)->P()[2]);
	  pts[1] = newPoints->InsertNextPoint(fi->V(1)->P()[0], fi->V(1)->P()[1], fi->V(1)->P()[2]);
	  pts[2] = newPoints->InsertNextPoint(fi->V(2)->P()[0], fi->V(2)->P()[1], fi->V(2)->P()[2]);
	  //fi->
	  newPolys->InsertNextCell(3, pts); //(vi->P()[0], vi->P()[1], vi->P()[2]);
  }

  /*
  for (i=0; i < this->CircumferentialResolution; i++)
    {
    for (j=0; j < this->RadialResolution; j++)
      {
      pts[0] = i*(this->RadialResolution+1) + j;
      pts[1] = pts[0] + 1;
      if ( i < (this->CircumferentialResolution-1) )
        {
        pts[2] = pts[1] + this->RadialResolution + 1;
        }
      else
        {
        pts[2] = j + 1;
        }
      pts[3] = pts[2] - 1;
      newPolys->InsertNextCell(4,pts);
      }
    }*/

  // Update ourselves and release memory
  //
  vtkSmartPointer<vtkPolyData> polyDataNotClean = vtkSmartPointer<vtkPolyData>::New();
  polyDataNotClean->SetPoints(newPoints);
  polyDataNotClean->SetPolys(newPolys);

  // Clean the polydata. This will remove duplicate points that may be
  // present in the input data.
  vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
  cleaner->SetInputData(polyDataNotClean);
  cleaner->Update();
  
  output->DeepCopy(cleaner->GetOutput());

  //output->SetPoints(newPoints);
  //newPoints->Delete();

  //output->SetPolys(newPolys);
  //newPolys->Delete();

  return 1;
}

  vtkImportVcgMesh::~vtkImportVcgMesh(void) {}

  void vtkImportVcgMesh::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "VcgMesh: " << this->VcgMesh << "\n";
}
