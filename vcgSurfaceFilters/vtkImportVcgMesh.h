
#ifndef vtkImportVcgMesh_h
#define vtkImportVcgMesh_h

#include <memory>

#include "vtkFiltersSourcesModule.h" // For export macro
#include "vtkPolyDataAlgorithm.h"
#include "vtkDiskSource.h"
#include "vtkSetGet.h"

#include "vcgTriMesh.h"

class vtkImportVcgMesh : public vtkPolyDataAlgorithm
{
public:
  typedef VcgTriMeshType VcgMeshType;
  static vtkImportVcgMesh *New();
  vtkTypeMacro(vtkImportVcgMesh,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Specify vcgMesh.

  //I am getting an unresolved external with using the set and get macros, not sure why
  //vtkSetMacro(VcgMesh,std::shared_ptr<VcgMeshType>);
  //GetMacro(VcgMesh,std::shared_ptr<VcgMeshType>);
  std::shared_ptr<VcgMeshType> VcgMesh;
  //OutputPointsPrecision
  

protected:
  vtkImportVcgMesh();
  //inline ~vtkImportVcgMesh() {};
  ~vtkImportVcgMesh();

  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  
private:
  vtkImportVcgMesh(const vtkImportVcgMesh&);  // Not implemented.
  void operator=(const vtkImportVcgMesh&);  // Not implemented.
};
#endif
